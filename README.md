# Scientific Ray Tracing
Project on ray tracing for scientific applications
## Cloning the repository
Make sure to clone **with submodules**: 
```
git clone --recursive https://gitlab.com/galerkin/scientific-ray-tracing.git
```
## Updating the repository
Make sure to pull **with submodules**:
```
git pull --recurse-submodules
```
