using LinearAlgebra
#using Strided

using BenchmarkTools

using Interpolations
using Profile
using ProfileView
using Images
using Colors
using Plots
using Revise
# Start modules that we want to use with Revise
using ScientificRT

num_x_voxels = 100;
num_y_voxels = 100;
num_z_voxels = 100;
N_x = num_x_voxels - 1 # number of x intervals
N_y = num_y_voxels - 1 # number of y intervals
N_z = num_z_voxels - 1 # number of z intervals
interval_width = (domain_len, num_intervals) -> domain_len/num_intervals
io = open("../../data/perlin_noise/test2_alt.raw", "r");
raw_data = Array{Float32, 1}(undef, num_x_voxels*num_y_voxels*num_z_voxels);
read!(io, raw_data);
volume = reshape(raw_data,num_x_voxels,num_y_voxels,num_z_voxels);

x_len = 1.0
y_len = 1.0
z_len = 1.0
x_step = interval_width(x_len,N_x)
y_step = interval_width(y_len,N_y)
z_step = interval_width(z_len,N_z)
bbox_min = [-0.5;-0.5;-0.5]
bbox_max = [0.5;0.5;0.5]
xs = bbox_min[1]:x_step:bbox_max[1]
ys = bbox_min[2]:y_step:bbox_max[2]
zs = bbox_min[3]:z_step:bbox_max[3]
vol_trilinear = LinearInterpolation((xs, ys, zs), volume, extrapolation_bc = 0.0);

# Camera aligned with cube
W = 640;
H = 480;
fovy = π/2;
eye_pos = [0.0;0.0;2.0];
up = [0.0;1.0;0.0];
view_dir = [0.0,0.0,-1.0];
pix_coords = pixel_indices(W,H);
raygen_func = get_raygen(W, H, fovy, eye_pos, up, view_dir);
rays = raygen_func.(pix_coords);

# test against bounding cube
cube_t_near = ray_cube_intersect.(rays);
cube_t_far = ray_cube_intersect.(rays, false);

#maximum intensity projection
@assert interval_width(x_len,N_x) == interval_width(y_len,N_y);
@assert interval_width(y_len,N_y) == interval_width(z_len,N_z);
step_size = interval_width(x_len,N_x)/2;
raymarch_MIP = get_raymarch_MIP(vol_trilinear, step_size, bbox_min, bbox_max);

#@profview MIP_result = raymarch_MIP.(rays, cube_t_near, cube_t_far);
@time MIP_result = raymarch_loop(raymarch_MIP, rays, cube_t_near, cube_t_far);
#heatmap(MIP_result)

MIP_result = raymarch_threaded(get_raymarch_MIP, vol_trilinear, step_size, bbox_min,
    bbox_max, rays, cube_t_near, cube_t_far);
heatmap(MIP_result)

test_ray = rays[200, 350]
test_t_near = cube_t_near[200, 350]
test_t_far = cube_t_far[200, 350]
raymarch_MIP(test_ray, test_t_near, test_t_far)
