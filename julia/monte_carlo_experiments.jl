using Distributions
using Plots

# ξ1 and ξ2 are random numbers drawn from a uniform distribution between
# 0 and 1
function sample_unit_disc(ξ1, ξ2)
	r = sqrt(ξ1)
	θ = 2π*ξ2
	return [r; θ]
end

function MC_estimate(f::Function,
					 pdf::Function,
					 draw_sample::Function,
					 N::Integer)
	sum = 0
	for i = 1:N
		X_i = draw_sample()
		sum += f(X_i)/pdf(X_i)
	end
	return sum/N
end

area_integrand1D = r -> 2π*r
area_integrand2D = λ -> λ[1] # assumes λ = [r; θ]

# Test my 1D integration example
pdf1D = λ -> 1.
uniform_dist = Uniform(0,1)
sampler = () -> rand(uniform_dist)
est1D = λ -> MC_estimate(area_integrand1D, pdf1D, sampler, λ)
area_estimates1D_100 = [est1D(100) for i in 1:100]
scatter(area_estimates1D_100, ylims =[2.64,3.64])
area_estimates1D_1000 = [est1D(1000) for i in 1:100]
scatter!(area_estimates1D_1000, ylims =[2.64,3.64])
area_estimates1D_10000 = [est1D(10000) for i in 1:100]
scatter!(area_estimates1D_10000, ylims =[2.64,3.64])
plot!( λ -> π, xlims=[0,100] )

# plot Monte Carlo convergence
N_range = 1:10:10000
rms_err = λ -> sqrt((λ-π)^2)
rms = [mean([rms_err(est1D(n)) for i in 1:100]) for n in N_range]
plot(N_range, rms, label="Monte carlo std. dev.")
plot!(λ -> 1/sqrt(λ), xlims=[1,10000], label="y=sqrt(x)")

# Test my 2D integration example
uniform_dist = Uniform(0,1)
function sampler2D()
    ξ1 = rand(uniform_dist)
    ξ2 = rand(uniform_dist)
    return sample_unit_disc(ξ1,ξ2)
end
pdf2D = λ -> λ[1]/π # assuming that λ = [r; θ]
est2D = λ -> MC_estimate(area_integrand2D, pdf2D, sampler2D, λ)
area_estimates2D_100 = [est2D(100) for i in 1:100]
