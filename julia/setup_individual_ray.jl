using Revise
push!(LOAD_PATH, pwd())
using VolRdrExperiments
using ScientificRT

vol_trilinear = setup_volume();
rays, cube_t_near, cube_t_far, tracker_array = get_arrays(vol_trilinear);
baseline_σ_s, baseline_σ_t, HG_samplers, L_light, max_bounces, light_dir,
  num_MC_iterations = pathtrace_params(rays)

test_X = 350
test_Y = 250
test_ray = rays[test_X, test_Y]
test_tracker =  tracker_array[test_X,test_Y]
test_t_near = cube_t_near[test_X,test_Y]
test_HG_sampler = HG_samplers[test_X,test_Y]
cached_ray = Ray([0.,0.,0.],[0.,0.,0.])
cached_scatter_pos = [0.,0.,0.] 
