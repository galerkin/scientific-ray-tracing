using LinearAlgebra
using Interpolations
using Plots

using Revise
using ScientificRT

num_x_voxels = 100;
num_y_voxels = 100;
num_z_voxels = 100;
N_x = num_x_voxels - 1 # number of x intervals
N_y = num_y_voxels - 1 # number of y intervals
N_z = num_z_voxels - 1 # number of z intervals
io = open("../data/perlin_noise/test2_alt.raw", "r");
raw_data = Array{Float32, 1}(undef, num_x_voxels*num_y_voxels*num_z_voxels);
read!(io, raw_data);
volume = reshape(raw_data,num_x_voxels,num_y_voxels,num_z_voxels);
#heatmap(volume[:,:,50], c=:viridis)

# Set up interpolation
x_len = 1.0
y_len = 1.0
z_len = 1.0
x_step = ScientificRT.interval_width(x_len,N_x)
y_step = ScientificRT.interval_width(y_len,N_y)
z_step = ScientificRT.interval_width(z_len,N_z)
bbox_min = [-0.5;-0.5;-0.5]
bbox_max = [0.5;0.5;0.5]
xs = bbox_min[1]:x_step:bbox_max[1]
ys = bbox_min[2]:y_step:bbox_max[2]
zs = bbox_min[3]:z_step:bbox_max[3]
vol_trilinear = LinearInterpolation((xs, ys, zs), volume, extrapolation_bc = 0.0);

test_ray = Ray(-[0.5;0.5;0.5],normalize([1.;1.;1.]))
test_volumeray = VolumeRay(test_ray, vol_trilinear)
probe_length = sqrt(3)
resolution = 1000
step_len = probe_length/resolution
line_probe_t = [t for t in 0:step_len:probe_length]
line_probe_data = [test_volumeray(t) for t in line_probe_t]
plot(line_probe_data)
