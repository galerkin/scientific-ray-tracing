# Results 5/18
* **Backscattering** means g=-0.35 for Henyey-Greenstein
* **Forward scattering** means g=0.67 for Henyey-Greenstein
* Light is a hemisphere, Ldir points towards the "top" of the dome
* Radiance of light is 1
## Values of σ_s and σ_a
* Subdirectories are named according to the convention `sXaY`. Here, `X` denotes the scattering coefficient σ_s, while `Y` denotes the absorption coefficient σ_a. For instance, results in the subdirectory `s80a10` denote results where σ_s is 80 and σ_a is 10.
