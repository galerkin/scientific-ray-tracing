using Revise
push!(LOAD_PATH, pwd())
using VolRdrExperiments

vol_info = setup_volume();
num_path_samples = 1e9
accum_buffer = count_scattering(vol_info, num_path_samples);
rel_freq = accum_buffer ./ num_path_samples 
