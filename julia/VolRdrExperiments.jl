module VolRdrExperiments

using LinearAlgebra
using Interpolations
using Printf
using Distributions
using FastClosures
using Printf

using Revise
using ScientificRT
import ScientificRT.pathtrace_ray!
import ScientificRT.pos_to_voxel_idx!

export setup_volume
export cast_rays_at_volume
export pathtrace!
export pathtrace_single_ray!
export pathtrace_params
export test_idx_func 
export count_scattering

# z_slice_val is a fixed z-position, since we are making a 2D plot 
function test_idx_func(z_slice_val, which_dim) 
  @assert which_dim == 1 || which_dim == 2 || which_dim == 3

  origin = [-0.5; -0.5; -0.5]

  L = [1.0; 1.0; 1.0] # length of each dimension of the volume
  N = [99; 99; 99] # num intervals in each dimension
  ℓ = L./N
  out_idxs = zeros(3)

  get_voxel_idx = @closure (x, y) -> begin
    z = z_slice_val 
    pos_to_voxel_idx!(out_idxs, [x; y; z], origin, ℓ, N)
    return out_idxs
  end

  plt_dims = [1000;1000]
  x_start = origin[1]
  x_end = origin[1] + L[1]
  x_step = (x_end - x_start)/plt_dims[1]
  y_start = origin[2]
  y_end = origin[2] + L[2]
  y_step = (y_end - y_start)/plt_dims[2]
  result = zeros(plt_dims[1]+1, plt_dims[2]+1)
  for (x_idx, x) in enumerate(x_start:x_step:x_end)
    for (y_idx, y) in enumerate(y_start:y_step:y_end)
      # @printf "(%f %f) at row=%d col=%d\n" x y x_idx y_idx      
      result[y_idx, x_idx] = get_voxel_idx(x, y)[which_dim]
    end
  end

  return result # Result can be visualized using heatmap()
end

struct VolInfo
  volume
  raw_data
  grid_dims
  origin
  ℓ
end

function setup_volume()
    num_x_voxels = 100;
    num_y_voxels = 100;
    num_z_voxels = 100;
    # Assuming data is cell-centered? Each vertex is at the *center* of a voxel
    N_x = num_x_voxels - 1 # number of x intervals
    N_y = num_y_voxels - 1 # number of y intervals
    N_z = num_z_voxels - 1 # number of z intervals
    io = open("../data/perlin_noise/test2_alt.raw", "r");
    raw_data = Array{Float32, 1}(undef, num_x_voxels*num_y_voxels*num_z_voxels);
    read!(io, raw_data);
    volume = reshape(raw_data,num_x_voxels,num_y_voxels,num_z_voxels);
    # Set up interpolation
    x_len = 1.0
    y_len = 1.0
    z_len = 1.0
    x_step = interval_width(x_len,N_x)
    y_step = interval_width(y_len,N_y)
    z_step = interval_width(z_len,N_z)
    bbox_min = [-0.5;-0.5;-0.5]
    bbox_max = [0.5;0.5;0.5]
    xs = bbox_min[1]:x_step:bbox_max[1]
    ys = bbox_min[2]:y_step:bbox_max[2]
    zs = bbox_min[3]:z_step:bbox_max[3]
    vol_trilinear = LinearInterpolation((xs, ys, zs), volume, 
                                        extrapolation_bc = 0.0);
    L = [x_len; y_len; z_len]
    N = [N_x; N_y; N_z]
    ℓ = L./N
    origin = bbox_min
    grid_dims = [num_x_voxels; num_y_voxels; num_z_voxels]
    return VolInfo(vol_trilinear, volume, grid_dims, origin, ℓ)
end

function cast_rays_at_volume(vol_trilinear)
    W = 512;
    H = 512;
    fovy = π/2;
    r_cam = 1.0
    θ_cam = 0;
    ϕ_cam = 0;
    tgt = [0.0;0.0;0.0]
    eye_pos= sph2cart(r_cam,θ_cam,ϕ_cam)
    view_dir = normalize(tgt - eye_pos)
    up = get_up(θ_cam, ϕ_cam)
    # Set raycast
    pix_coords = pixel_indices(W,H);
    raygen_func = get_raygen(W, H, fovy, eye_pos, up, view_dir);
    rays = raygen_func.(pix_coords);

    cube_t_near = ray_cube_intersect.(rays);
    cube_t_far = ray_cube_intersect.(rays, false);

    #=
     =spawn_vol_ray = ray -> VolumeRay(ray, vol_trilinear)
     =volume_rays = spawn_vol_ray.(rays)
     =σ_t_max = 1. # HACK: hardcoding based on a priori knowledge of the volume
     =spawn_tracker = σ -> WoodcockTracker(σ,σ_t_max)
     =tracker_array = spawn_tracker.(volume_rays);
     =#

    return rays, cube_t_near, cube_t_far#, tracker_array
end

#=
 =function pathtrace_params(rays)
 =    baseline_σ_a = 10
 =    baseline_σ_s = 80
 =    baseline_σ_t = baseline_σ_a + baseline_σ_s # σ_t = σ_a + σ_s
 =    L_light = 1.0
 =    max_bounces = 50
 =    #g = 0.67
 =    g = -0.35
 =    g_array = g*ones(size(rays))
 =    HG_samplers = get_HG_direction_sampler.(g_array)
 =    num_MC_iterations = 2000
 =    light_dir = [0.0;0.0;1.0]
 =    
 =    return baseline_σ_s, 
 =           baseline_σ_t, 
 =           HG_samplers, 
 =           L_light, 
 =           max_bounces, 
 =           light_dir, 
 =           num_MC_iterations
 =end
 =#

function pathtrace!(result,
                    ray_cache,
                    scatter_pos_cache,
                    rays,
                    cube_t_near,
                    cube_t_far,
                    tracker_array)

  baseline_σ_s, baseline_σ_t, HG_samplers, L_light, max_bounces, light_dir,
    num_MC_iterations = pathtrace_params(rays);

    pathtrace_rays!(result,
                    ray_cache,
                    scatter_pos_cache,
                    rays,
                    baseline_σ_s,
                    baseline_σ_t,
                    tracker_array,
                    HG_samplers,
                    cube_t_near,
                    L_light,
                    max_bounces,
                    light_dir,
                    num_MC_iterations)
end

function count_scattering(vol_info, num_path_samples)
  num_threads = Threads.nthreads()
  vol = vol_info.volume
  grid_dims = vol_info.grid_dims
  origin = vol_info.origin
  ℓ = vol_info.ℓ

  emitter_pos = [0.0; 0.0; 0.0]
  g = 0.67
  # g = -0.35

  cs_dat = get_countscatter_data(num_threads, grid_dims, emitter_pos, g, vol)

  # FIXME: remove duplicated code that also occurs in the pathtrace_params()
  # function / find a way to separate data and code
  
  baseline_σ_a = 10
  baseline_σ_s = 80
  baseline_σ_t = baseline_σ_a + baseline_σ_s # σ_t = σ_a + σ_s
  max_bounces = 50
   
  count_scattering_events!( cs_dat, 
                            baseline_σ_s, 
                            baseline_σ_t, 
                            origin, 
                            ℓ, 
                            max_bounces,
                            num_path_samples )

  return cs_dat.accum_buffer
end

#=
 =function pathtrace_single_ray!(screen_coords,
 =                               ray_cache,
 =                               scatter_pos_cache,
 =                               rays,
 =                               cube_t_near,
 =                               tracker_array)
 =
 =  baseline_σ_s, baseline_σ_t, HG_samplers, L_light, max_bounces, light_dir,
 =    num_MC_iterations = pathtrace_params(rays)
 =
 =  test_X = screen_coords[1]
 =  test_Y = screen_coords[2]
 =  test_cached_ray = ray_cache[test_X, test_Y]
 =  test_start_ray = rays[test_X, test_Y]
 =  test_scatter_pos = scatter_pos_cache[test_X, test_Y]
 =  test_tracker = tracker_array[test_X,test_Y]
 =  test_t_near = cube_t_near[test_X,test_Y]
 =  test_phase_sampler! = HG_samplers[test_X,test_Y]
 =  return pathtrace_ray!(test_cached_ray,
 =                        test_scatter_pos,
 =                        test_start_ray,
 =                        baseline_σ_s,
 =                        baseline_σ_t,
 =                        test_tracker,
 =                        test_phase_sampler!,
 =                        test_t_near,
 =                        L_light,
 =                        light_dir,
 =                        max_bounces)
 =end
 =#

end # module VolRdrExperiments
