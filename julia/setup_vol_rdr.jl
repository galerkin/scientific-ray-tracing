using Revise
using ScientificRT
push!(LOAD_PATH, pwd())
using VolRdrExperiments

baseline_σ_a = 10
baseline_σ_s = 80
baseline_σ_t = baseline_σ_a + baseline_σ_s # σ_t = σ_a + σ_s
σ_t_max = 1. # HACK: hardcoding based on a priori knowledge of the volume
L_light = 1.0
max_bounces = 50
# g = 0.67
g = -0.35
MC_iterations = 2000

vol_info = setup_volume();
start_rays, cube_t_near, cube_t_far = cast_rays_at_volume(vol_info.volume)
ss_info = ScatterSimInfo(baseline_σ_s,
                         baseline_σ_t,
                         g,
                         max_bounces,
                         vol_info.grid_dims,
                         vol_info.origin,
                         vol_info.ℓ)
# TODO: Feed the above data to the PathTracer constructor
pt = PathTracer(ss_info,
                vol_info.volume,
                start_rays,
                cube_t_near,
                σ_t_max,
                MC_iterations)

emitter_pos = [0.0; 0.0; 0.0]
num_path_samples = 1000
counter = CountScatter(ss_info, vol_info.volume, emitter_pos, num_path_samples)

num_transmit_trials = 100
tr_est = TransmittanceEstimator(start_rays,
                                vol_info.volume,
                                cube_t_near,
                                cube_t_far,
                                num_transmit_trials)

