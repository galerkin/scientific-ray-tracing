# From tutorial at:
# https://julialang.org/blog/2019/07/multithreading/

import Base.Threads.@spawn
using Plots

function fib(n::Int)
    if n < 2
        return n
    end
    t = @spawn fib(n-2)
    return fib(n-1) + fetch(t)
end

function invoke_apply_parallel!(f::Function,
                                serial_cutoff,
                                out_result,
                                f_in...)
    # Assumption: length of all input arrays in f_in are the same
    apply_parallel!(f, serial_cutoff, 1, length(f_in[1]), out_result, f_in...)
end

# Based on the "mergesort" code sample at:
# https://julialang.org/blog/2019/07/multithreading/
function apply_parallel!(f::Function,
                         serial_cutoff::Int,
                         lo::Int,
                         hi::Int,
                         out_result,
                         f_in...) # f_inputs is a tuple (varargs)
    if lo >= hi # 1 or 0 elements, nothing to do
        return
    end
    if hi - lo < serial_cutoff # below some cutoff, run in serial
        sliced_inputs = [view(x,lo:hi) for x in f_in]
        out_result[lo:hi] = f.(sliced_inputs...)
        return
    end

    mid = (lo+hi)>>>1 # find the midpoint
    @spawn apply_parallel!(f, serial_cutoff, lo, mid, out_result, f_in...)
    apply_parallel!(f, serial_cutoff, mid+1, hi, out_result, f_in...)
end

x_array = [x for x in 1:262144]
y_array = [rand() for i in 1:262144]
result = zeros(size(x_array))

myfunc = λ->λ^2

function myfunc_2D(x,y)
    return x^2 + 500*y
end

#invoke_apply_parallel!(myfunc, 8, result, x_array)
invoke_apply_parallel!(myfunc_2D, 4096, result, x_array, y_array)
plot(result)
